package logger

// Config holds the configuration for a logger
type Config struct {
	staticFields Extra
	serializers  map[string]Serializer
}

// Logger implements functions to perform logging
type Logger struct {
	config Config
}

// Extra can be used as shorthand for log.x() calls
type Extra map[string]interface{}

func log(l *Logger, level LogLevel, msg string, staticArgs Extra, args ...interface{}) {
	if level < currentLogLevel {
		return
	}
	var pieces []string
	if textOutput {
		writeText(level, msg)
	} else {
		pieces = []string{
			jsonKeyPair("time", nowTimestamp()),
			jsonKeyPair("levelName", nameOfLevel(level)),
			jsonKeyPair(moduleKey, l.config.staticFields[moduleKey]),
			jsonKeyPair("msg", msg),
			jsonKeyPair("level", level),
		}
	}
	for k, v := range l.config.staticFields {
		if k == moduleKey {
			continue
		}
		v = processStatic(l.config.serializers, v)
		pieces = addJSONPiece(pieces, k, v)
	}
	for k, v := range staticArgs {
		v = processStatic(l.config.serializers, v)
		pieces = addJSONPiece(pieces, k, v)
	}
	for _, arg := range args {
		k, v, _ := serialize(l.config.serializers, arg)
		pieces = addJSONPiece(pieces, k, v)
	}

	writeJSON(pieces)
}

// Fatal logs a fatal log entry
func (l *Logger) Fatal(msg string, staticArgs Extra, args ...interface{}) {
	log(l, levels["fatal"], msg, staticArgs, args...)
}

// Error logs an error log entry
func (l *Logger) Error(msg string, staticArgs Extra, args ...interface{}) {
	log(l, levels["error"], msg, staticArgs, args...)
}

// Warn logs a warning log entry
func (l *Logger) Warn(msg string, staticArgs Extra, args ...interface{}) {
	log(l, levels["warn"], msg, staticArgs, args...)
}

// Info logs a info log entry
func (l *Logger) Info(msg string, staticArgs Extra, args ...interface{}) {
	log(l, levels["info"], msg, staticArgs, args...)
}

// Debug logs a debug log entry
func (l *Logger) Debug(msg string, staticArgs Extra, args ...interface{}) {
	log(l, levels["debug"], msg, staticArgs, args...)
}

// Trace logs a trace log entry
func (l *Logger) Trace(msg string, staticArgs Extra, args ...interface{}) {
	log(l, levels["trace"], msg, staticArgs, args...)
}
