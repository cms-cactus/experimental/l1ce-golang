package logger

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/logrusorgru/aurora"
)

var stdSerializers = map[string]Serializer{
	"err": func(maybeErr interface{}) (interface{}, bool) {
		if err, ok := maybeErr.(error); ok {
			return err.Error(), true
		}
		return nil, false
	},
}

// Serializer defines a function that turns a value of a certain type into a loggable entity
type Serializer func(value interface{}) (interface{}, bool)

func serialize(serializers map[string]Serializer, value interface{}) (string, interface{}, bool) {
	for key, serializer := range serializers {
		if serialized, ok := serializer(value); ok {
			return key, serialized, true
		}
	}
	return fmt.Sprintf("%T", value), fmt.Sprintf("%s", value), false
}

func nowTimestamp() string {
	return time.Now().UTC().Format("2006-01-02T15:04:05.999Z07:00")
}

func processStatic(serializers map[string]Serializer, value interface{}) interface{} {
	if strv, ok := value.(string); ok {
		return strv
	}
	_, parsed, _ := serialize(serializers, value)
	return parsed
}

func jsonKeyPair(key string, value interface{}) string {
	k, err := json.Marshal(key)
	if err != nil {
		return ""
	}
	v, err := json.Marshal(value)
	if err != nil {
		return ""
	}
	return string(k) + ":" + string(v)
}

func coloredLevel(l LogLevel) string {
	var r aurora.Value
	s := nameOfLevel(l)
	if l >= levels["fatal"] {
		r = aurora.BgRed(aurora.Black(s))
	} else if l >= levels["error"] {
		r = aurora.Red(s)
	} else if l >= levels["warn"] {
		r = aurora.Yellow(s)
	} else if l >= levels["info"] {
		r = aurora.Blue(s)
	} else {
		return s
	}
	return r.String()
}

func addJSONPiece(pieces []string, key string, value interface{}) []string {
	if s := jsonKeyPair(key, value); s != "" {
		return append(pieces, s)
	}
	return pieces
}

func writeJSON(pieces []string) {
	write("{" + strings.Join(pieces, ",") + "}\n")
}

func writeText(level LogLevel, msg string) {
	l := len(nameOfLevel(level))
	padding := maxLevelLength - l
	write(coloredLevel(level) + strings.Repeat(" ", padding) + ": " + msg + " ")
}

func write(s string) {
	// this app has a CLI, therefore all non-application
	// output (including log messages) should go to stderr
	f := os.Stderr
	f.WriteString(s)
}
