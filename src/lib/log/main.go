package logger

// LogLevel is a logging level
type LogLevel int

var levels = map[string]LogLevel{
	"fatal": 60,
	"error": 50,
	"warn":  40,
	"info":  30,
	"debug": 20,
	"trace": 10,
}

func reverseLevels() map[LogLevel]string {
	r := make(map[LogLevel]string)
	for k, v := range levels {
		r[v] = k
	}
	return r
}

func biggestLevelName() int {
	r := 0
	for _, v := range levels {
		l := len(nameOfLevel(v))
		if l > r {
			r = l
		}
	}
	return r
}

var levelsR = reverseLevels()
var maxLevelLength = biggestLevelName()

var textOutput = false

const moduleKey = "module"

var rootConfig = Config{
	staticFields: make(Extra),
	serializers:  stdSerializers,
}

var currentLogLevel = levels["info"]

// SetLogLevel sets the global minimum log level that is to be outputted
// any log() calls that have a lower level than specified here
// will be discarded
func SetLogLevel(level string) bool {
	l := levels[level]
	if l != 0 {
		currentLogLevel = l
	}
	return l != 0
}

// SetLogFormat to either 'JSON', or 'text'
func SetLogFormat(format string) bool {
	if format == "text" {
		textOutput = true
		return true
	} else if format == "JSON" {
		textOutput = false
		return true
	}
	return false
}

// GetLogLevel returns the current loglevel
func GetLogLevel() string {
	return nameOfLevel(currentLogLevel)
}

func nameOfLevel(l LogLevel) string {
	return levelsR[l]
}

// Init initializes logger with general configuration
func Init(c Config, appName string) Config {
	for k, v := range c.staticFields {
		rootConfig.staticFields[k] = v
	}
	rootConfig.staticFields["app"] = appName
	for k, v := range c.serializers {
		rootConfig.serializers[k] = v
	}

	for _, logger := range loggerCache {
		c := logger.config
		for k, v := range rootConfig.staticFields {
			if c.staticFields[k] == nil {
				c.staticFields[k] = v
			}
		}
		for k, v := range rootConfig.serializers {
			if c.serializers[k] == nil {
				c.serializers[k] = v
			}
		}
	}

	return rootConfig
}

var loggerCache = make([]Logger, 0)

// GetLogger gives a functional logging instance with the given module namespace
func GetLogger(c Config, moduleName string) Logger {
	config := Config{
		staticFields: make(Extra),
		serializers:  make(map[string]Serializer),
	}

	for k, v := range rootConfig.staticFields {
		config.staticFields[k] = v
	}
	for k, v := range c.staticFields {
		config.staticFields[k] = v
	}
	config.staticFields[moduleKey] = moduleName

	for k, v := range rootConfig.serializers {
		config.serializers[k] = v
	}
	for k, v := range c.serializers {
		config.serializers[k] = v
	}

	l := Logger{config}
	loggerCache = append(loggerCache, l)

	return l
}
