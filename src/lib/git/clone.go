package git

import (
	"errors"
	"os"
	"path/filepath"
	"strings"

	ConfKeys "../config"
	l "../log"
	Static "../static"
	// "golang.org/x/crypto/ssh/terminal"
)

// CloneConfig for use with clone()
type CloneConfig struct {
	remote    string
	localpath string
	username  string
	pass      string
}

// DefaultCloneConfig builds a CloneConfig from viper values
func DefaultCloneConfig() (*CloneConfig, error) {
	remote, err := ConfKeys.GetString(ConfKeys.GitRepository)
	if err != nil {
		return nil, err
	}
	local, err := ConfKeys.GetString(ConfKeys.GitClonePath)
	if err != nil {
		return nil, err
	}
	// isTerminal := terminal.IsTerminal(int(os.Stdout.Fd()))
	gitUser, err := ConfKeys.GetString(ConfKeys.GitUser)
	if err != nil {
		return nil, err
	}
	gitAPIKey, err := ConfKeys.GetString(ConfKeys.GitAPIKey)
	if err != nil {
		return nil, err
	}
	return &CloneConfig{remote, local, gitUser, gitAPIKey}, nil
}

// CheckCloneConfig validates a CloneConfig
func CheckCloneConfig(c *CloneConfig) error {
	if c == nil {
		return errors.New("no clone config")
	}
	if c.localpath == "" {
		return errors.New("no local path")
	}
	if !strings.Contains(c.remote, "://") || !strings.HasSuffix(c.remote, ".git") {
		return errors.New("not a valid remote")
	}
	return nil
}

// Clone initializes a new repository. this means
// - cloning the repository
// - pulling in git-notes
func Clone(c *CloneConfig) error {
	if c == nil {
		var err error
		c, err = DefaultCloneConfig()
		if err != nil {
			return err
		}
	}

	if err := CheckCloneConfig(c); err != nil {
		return err
	}
	privilegedRemote := ""
	if c.username != "" && c.pass != "" {
		pieces := strings.Split(c.remote, "://")
		if len(pieces) > 1 {
			privilegedRemote = pieces[0] + "://" + c.username + ":" + c.pass + "@" + pieces[1]
		}
	}
	if privilegedRemote == "" {
		privilegedRemote = c.remote
	}
	if _, err := os.Stat(filepath.Dir(c.localpath)); err != nil {
		return errors.New("parent folder for configured destination does not exist")
	}
	if _, err := os.Stat(c.localpath); err == nil {
		return errors.New("clone path already exists")
	}
	if err := os.Mkdir(c.localpath, os.ModePerm); err != nil {
		return errors.New("cannot create " + c.localpath)
	}
	masterPath := c.localpath + "/master"

	log.Info("cloning repository", l.Extra{"repository": c.remote, "path": masterPath})
	stdout, stderr, err := Static.ExecuteScript("git/clone.sh", privilegedRemote, masterPath)
	if err != nil {
		log.Error("clone script failed", l.Extra{"stdout": stdout, "stderr": stderr}, err)
		return err
	}
	log.Info("clone script successful", l.Extra{"stdout": stdout, "stderr": stderr})
	return nil
}
