package git

import (
	"os"
	"path/filepath"

	l "../log"
	Static "../static"
)

// MakeWorkspace creates a new workspace from a given master folder
func MakeWorkspace(c *WorkspaceConfig) error {
	if err := CheckWsConfig(c); err != nil {
		return err
	}

	to := filepath.Dir(c.master) + "/" + c.name
	if _, err := os.Stat(to); err == nil {
		log.Warn("Workspace already exists", l.Extra{"path": to, "branch": c.name})
		return nil
	}

	log.Info("creating new workspace", l.Extra{"to": to, "from": c.master, "branch": c.name})

	stdout, stderr, err := Static.ExecuteScript("git/create_workspace.sh", c.master, to, c.name)
	if err != nil {
		log.Error("workspace creation script failed", l.Extra{"stdout": stdout, "stderr": stderr}, err)
		return err
	}
	log.Info("workspace creation script successful", l.Extra{"stdout": stdout, "stderr": stderr})
	return nil
}
