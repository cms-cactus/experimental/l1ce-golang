package git

import (
	"errors"

	ConfKeys "../config"
)

// WorkspaceConfig for use in workspace commands
type WorkspaceConfig struct {
	master string
	name   string
}

// WsFromDefaultMaster makes a WorkspaceConfig from viper values
func WsFromDefaultMaster(name string) (*WorkspaceConfig, error) {
	remote, err := DefaultWsMaster()
	if err != nil {
		return nil, err
	}
	return &WorkspaceConfig{remote, name}, nil
}

// CheckWsConfig checks a WorkspaceConfig
func CheckWsConfig(c *WorkspaceConfig) error {
	if c == nil {
		return errors.New("no workspace config")
	}
	if c.master == "" {
		return errors.New("no workspace master")
	}
	if c.name == "" {
		return errors.New("no workspace name")
	}
	return nil
}

// DefaultWsMaster gives the default workspace master from viper values
func DefaultWsMaster() (string, error) {
	basePath, err := ConfKeys.GetString(ConfKeys.GitClonePath)
	if err != nil {
		return "", err
	}
	return basePath + "/master", nil
}
