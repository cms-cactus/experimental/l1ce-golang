package git

import (
	"errors"
	"os"
	"path/filepath"

	l "../log"
	Static "../static"
)

// DeleteWorkspace deletes workspace and removes the branch with that name
func DeleteWorkspace(c *WorkspaceConfig) error {
	if err := CheckWsConfig(c); err != nil {
		return err
	}

	to := filepath.Dir(c.master) + "/" + c.name
	if _, err := os.Stat(to); err != nil {
		return errors.New("workspace does not exists")
	}
	log.Info("deleting workspace", l.Extra{"workspace path": to, "from": c.master, "branch": c.name})

	stdout, stderr, err := Static.ExecuteScript("git/delete_workspace.sh", c.master, to, c.name)
	if err != nil {
		log.Error("workspace deletion script failed", l.Extra{"stdout": stdout, "stderr": stderr}, err)
		return err
	}
	log.Info("workspace deletion script successful", l.Extra{"stdout": stdout, "stderr": stderr})
	return nil
}
