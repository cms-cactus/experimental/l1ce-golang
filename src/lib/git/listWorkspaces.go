package git

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	l "../log"
	Static "../static"
)

// Workspace entry
type Workspace struct {
	Master string
	Name   string
	Path   string
	Dirty  bool
}

// ListWorkspaces returns a list of current workspace entries
func ListWorkspaces(master string) ([]Workspace, error) {
	if master == "" {
		return nil, errors.New("no master given")
	}
	basePath := filepath.Dir(master)
	files, err := ioutil.ReadDir(basePath)
	if err != nil {
		return nil, err
	}
	workspaces := make([]Workspace, 0)
	for _, file := range files {
		if file.IsDir() {
			workspaceName := file.Name()
			workspacePath := basePath + "/" + workspaceName
			gitConfigFile := workspacePath + "/.git/config"
			if _, err := os.Stat(gitConfigFile); err == nil {
				// if it is a workspace, the git config file is a symlink
				if configInfo, err := os.Lstat(gitConfigFile); err == nil && configInfo.Mode()&os.ModeSymlink == os.ModeSymlink {
					if symlinkTarget, err := os.Readlink(gitConfigFile); err == nil {
						// and the symlink must point to the master
						if strings.HasSuffix(symlinkTarget, master+"/.git/config") {
							stdout, stderr, err := Static.ExecuteScript("git/isdirty.sh", workspacePath)
							if err != nil {
								log.Error("workspace status script failed", l.Extra{"stdout": stdout, "stderr": stderr}, err)
								return nil, err
							}
							dirty := false
							if stdout == "clean" {
								dirty = false
							} else if stdout == "dirty" {
								dirty = true
							} else {
								return nil, errors.New("invalid stdout from script: " + stdout)
							}
							workspaces = append(workspaces, Workspace{master, workspaceName, workspacePath, dirty})
						}
					}
				}
			}
		}
	}
	return workspaces, nil
}
