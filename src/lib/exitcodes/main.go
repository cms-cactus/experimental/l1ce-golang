package exitcodes

// exit codes, stolen from
// https://www.kennethreitz.org/essays/unix-exit-status-code-reference
// https://shapeshed.com/unix-exit-codes/
// https://stackoverflow.com/questions/1101957/are-there-any-standard-exit-status-codes-in-linux
const (
	Success               = 0
	GenericFailure        = 1
	CLIUsageError         = 64
	DataFormatError       = 65
	CannotOpenInput       = 66
	AddresseeUnknown      = 67
	HostnameUnknown       = 68
	ServiceUnavailable    = 69
	InternalError         = 70
	SystemError           = 71
	CriticalOSFileMissing = 72
	CannotCreateFile      = 73
	IOError               = 74
	TemporaryFailure      = 75
	RemoteProtocolError   = 76
	PermissionError       = 77
	ConfigError           = 78
	CtrlC                 = 130
)
