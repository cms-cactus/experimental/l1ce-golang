package database

import (
	config "../config"
	l "../log"
)

func TestConnections() error {
	users, err := config.GetStringSlice(config.DBUsers)
	if err != nil {
		return err
	}
	for _, user := range users {
		log.Info("connecting to db", l.Extra{"username": user})
		connection, err := Connect(user)
		if err != nil {
			return err
		}
		log.Info("connection successfull", l.Extra{"username": user})
		err = connection.Close()
		if err != nil {
			return err
		}
		log.Debug("connection closed", l.Extra{"username": user})
	}
	return nil
}
