package database

import (
	"errors"
	"strconv"
	"strings"

	config "../config"
	l "../log"
)

// FkDefinition describes a foreign key
type FkDefinition struct {
	RemoteTable  string `yaml:"table"`
	RemoteColumn string `yaml:"column"`
}

// ColumnDefinition describes a db table column
type ColumnDefinition struct {
	DataType     string
	MaxLength    int
	Nullable     bool
	DefaultValue string       `yaml:"default,omitempty"`
	Fk           FkDefinition `yaml:"foreignkey,omitempty"`
}

// TableColumns lists the columns of a table
type TableColumns struct {
	Primary     string `yaml:",omitempty"`
	Mode        string `yaml:",omitempty"`
	Author      string `yaml:",omitempty"`
	Definitions map[string]ColumnDefinition
}

// GetTableColumns generates a TableColumns struct
func GetTableColumns(tablename string) (TableColumns, error) {
	if tablename == "" {
		return TableColumns{}, errors.New("no tablename given to getTableColumns")
	}
	if tableColumsCache[tablename].Primary != "" {
		return tableColumsCache[tablename], nil
	}
	fancySQL := "select column_name, data_type, data_length, nullable, data_default from all_tab_cols where table_name = :1 and owner in " + MakeLegalOwnersSQLSet()
	allColumns, err := QueryToMaps(tablename, fancySQL, tablename)
	if err != nil {
		return TableColumns{}, err
	}

	fkDefinitions, err := getForeignKeyDefinitions(tablename)
	if err != nil {
		return TableColumns{}, err
	}

	primaryColumn, err := getPrimaryColumn(tablename)
	if err != nil {
		return TableColumns{}, err
	}

	columnDefs := make(map[string]ColumnDefinition)
	var modeColumn, authorColumn string
	for _, row := range allColumns {
		columnName := row["COLUMN_NAME"]
		if strings.HasPrefix(columnName, "SYS_") && columnName[len(columnName)-1:] == "$" {
			continue
		}
		maxlength, err := strconv.Atoi(row["DATA_LENGTH"])
		if err != nil {
			return TableColumns{}, err
		}
		columnDefs[columnName] = ColumnDefinition{
			DataType:     row["DATA_TYPE"],
			MaxLength:    maxlength,
			Nullable:     row["NULLABLE"] == "Y",
			DefaultValue: row["DATA_DEFAULT"],
			Fk:           fkDefinitions[columnName],
		}
		if b, err := isModeColumn(columnName); err != nil {
			return TableColumns{}, err
		} else if b && fkDefinitions[columnName].RemoteTable != "" {
			modeColumn = columnName
		}
		if b, err := isAuthorColumn(columnName); err != nil {
			return TableColumns{}, err
		} else if b {
			authorColumn = columnName
		}
	}
	tableColumsCache[tablename] = TableColumns{primaryColumn, modeColumn, authorColumn, columnDefs}
	return tableColumsCache[tablename], nil
}

func getForeignKeyDefinitions(tableName string) (map[string]FkDefinition, error) {
	if foreignKeyCache[tableName] != nil {
		return foreignKeyCache[tableName], nil
	}
	fkSQL :=
		`select acc1.table_name,
	     acc1.column_name,
	     acc2.table_name as r_table_name,
	     acc2.column_name as r_column_name
from all_constraints ac
inner join all_cons_columns acc1 on ac.constraint_name = acc1.constraint_name
inner join all_cons_columns acc2 on ac.r_constraint_name = acc2.constraint_name
where ac.table_name = :1
	and ac.constraint_type = 'R'
	and ac.status = 'ENABLED'
	and ac.owner in ` + MakeLegalOwnersSQLSet() + `
order by column_name`
	defaultUser, err := config.GetString(config.DBDefaultUser)
	if err != nil {
		return nil, err
	}
	conn, err := Connect(defaultUser)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	result, err := conn.Query(fkSQL, tableName)
	if err != nil {
		return nil, err
	}
	defer result.Close()

	fkData := RowsToMaps(result)
	fkData2 := make(map[string]FkDefinition)
	for _, r := range fkData {
		fkData2[r["COLUMN_NAME"]] = FkDefinition{r["R_TABLE_NAME"], r["R_COLUMN_NAME"]}
	}
	foreignKeyCache[tableName] = fkData2
	return fkData2, nil
}

func getPrimaryColumn(tableName string) (string, error) {
	if primaryColumnCache[tableName] != "" {
		return primaryColumnCache[tableName], nil
	}
	fancySQL := `select acc.column_name, ac.owner
from all_constraints ac
inner join all_cons_columns acc on ac.constraint_name = acc.constraint_name
where ac.table_name = :1 
		and ac.constraint_type = 'P'
		and ac.owner IN ` + MakeLegalOwnersSQLSet()

	defaultUser, err := config.GetString(config.DBDefaultUser)
	if err != nil {
		return "", err
	}
	conn, err := Connect(defaultUser)
	if err != nil {
		log.Error("cannot get primary column", l.Extra{"table": tableName}, err)
		return "", err
	}
	defer conn.Close()

	columnName := ""
	owner := ""
	rows, err := conn.Query(fancySQL, tableName)
	if err != nil {
		return "", err
	}
	for rows.Next() {
		rows.Scan(&columnName, &owner)
	}
	if columnName == "" {
		return "", errors.New("no primary key found for table " + tableName)
	}
	primaryColumnCache[tableName] = columnName
	return columnName, nil

}
