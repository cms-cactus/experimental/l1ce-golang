package database

import (
	"database/sql"
	"errors"
	"fmt"

	config "../config"

	_ "gopkg.in/goracle.v2" // goracle driver
)

// Connect connects to database with given username
func Connect(username string) (*sql.DB, error) {
	pass, err := getDBCredentials(username)
	if err != nil || pass == "" {
		var reason string
		if err != nil {
			reason = err.Error()
		} else {
			reason = "empty password"
		}
		return nil, errors.New("credentials for " + username + " not configured: " + reason)
	}
	dbname, err := config.GetString(config.DBDefault)
	if err != nil {
		return nil, err
	}
	aliases, err := getAliases()
	if err != nil {
		return nil, err
	}
	if aliases[dbname] != "" {
		dbname = aliases[dbname]
	}
	conn, err := sql.Open("goracle", username+"/"+pass+"@"+dbname)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

var aliases map[string]string

func getAliases() (map[string]string, error) {
	if aliases != nil {
		return aliases, nil
	}
	aliases = make(map[string]string)
	aliasesInterface, err := config.Get(config.DBAliases)
	if err != nil {
		return nil, err
	}
	var aliasList []interface{}
	var ok bool
	if aliasList, ok = aliasesInterface.([]interface{}); !ok {
		return nil, config.WrongConfigError(config.DBAliases, "not an array")
	}
	for _, aliasListItem := range aliasList {
		var aliasMap map[interface{}]interface{}
		if aliasMap, ok = aliasListItem.(map[interface{}]interface{}); !ok {
			return nil, config.WrongConfigError(config.DBAliases, "not a map")
		}
		alias := fmt.Sprintf("%v", aliasMap["alias"])
		db := fmt.Sprintf("%v", aliasMap["database"])
		aliases[alias] = db
	}
	return aliases, nil
}
