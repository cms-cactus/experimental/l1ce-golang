package database

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	config "../config"
	l "../log"

	_ "gopkg.in/goracle.v2" // goracle driver
)

var log = l.GetLogger(l.Config{}, "db")

// MakeLegalOwnersSQLSet returns an SQL array of owners for use in SQL queries
func MakeLegalOwnersSQLSet() string {
	legalOwners, err := config.GetStringSlice(config.DBTableOwners)
	if err != nil {
		return ""
	}
	wrapped := make([]string, len(legalOwners))
	for i, o := range legalOwners {
		wrapped[i] = "'" + strings.ToUpper(o) + "'"
	}
	return "( " + strings.Join(wrapped, ", ") + " )"
}

// QueryToMaps runs a query and returns a map
func QueryToMaps(tableName string, query string, queryArgs ...interface{}) ([]map[string]string, error) {
	owner, accessor, err := GetTableOwner(tableName)
	if err != nil {
		return nil, err
	}
	query = strings.ReplaceAll(query, tableName, owner+"."+tableName)

	conn, err := Connect(accessor)
	if err != nil {
		log.Error("cannot connect to db", l.Extra{"owner": owner}, err)
		return nil, err
	}
	defer conn.Close()

	rows, err := conn.Query(query, queryArgs...)
	if err != nil {
		log.Error("query failed", l.Extra{"query": query}, err)
		return nil, err
	}
	defer rows.Close()

	return RowsToMaps(rows), nil
}

// RowsToMaps converts *Rows reference to a usable map
func RowsToMaps(rows *sql.Rows) []map[string]string {
	columns, _ := rows.Columns()
	result := make([]map[string](string), 0)
	for rows.Next() {
		row := make([]interface{}, len(columns))
		rowPointers := make([]interface{}, len(columns))
		for i := range row {
			rowPointers[i] = &row[i]
		}
		rows.Scan(rowPointers...)
		rowMap := make(map[string]string)
		for i, columnName := range columns {
			val := fmt.Sprintf("%v", row[i])
			if t, ok := row[i].(time.Time); ok {
				val = t.UTC().Format("2006-01-02 15:04:05")
			}
			rowMap[columnName] = val
		}
		result = append(result, rowMap)
	}
	return result
}
