package database

import (
	"io/ioutil"
	"strings"

	config "../config"
	_ "gopkg.in/goracle.v2" // goracle driver
)

func getDBCredentials(username string) (string, error) {
	dbName, err := config.GetString(config.DBDefault)
	if err != nil {
		return "", err
	}
	credentialsType, err := config.GetString(config.DBCredentialsType)
	if err != nil {
		return "", err
	}
	if credentialsType != "file" {
		err := config.WrongConfigError(config.DBCredentialsType, "must be 'file'")
		return "", err
	}
	credentialsPath, err := config.GetString(config.DBCredentialsPath)
	if err != nil {
		return "", err
	}
	credentialsPath = strings.ReplaceAll(credentialsPath, "%name%", username)
	credentialsPath = strings.ReplaceAll(credentialsPath, "%dbname%", dbName)
	data, err := ioutil.ReadFile(credentialsPath)
	if err != nil {
		return "", err
	}
	pass := strings.Replace(string(data), "\n", "", 1)
	return pass, nil
}
