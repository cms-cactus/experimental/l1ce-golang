package database

import (
	config "../config"

	_ "gopkg.in/goracle.v2" // goracle driver
)

// IsIgnoredColumn determines if the given columnname is to be ignored
func IsIgnoredColumn(tablename string, columnName string) (bool, error) {
	ignoredColumns, err := config.GetStringSlice(config.DBIgnoredColumns)
	if err != nil {
		return false, err
	}
	for _, c := range ignoredColumns {
		if c == columnName {
			return true, nil
		}
	}

	tableColumns, err := GetTableColumns(tablename)
	if err != nil {
		return false, err
	}
	if tableColumns.Author == columnName {
		return true, nil
	}
	return tableColumns.Mode == columnName, nil
}

// isModeColumn determines if the given columnname is a mode column
func isModeColumn(columnName string) (bool, error) {
	modeColumns, err := config.GetStringSlice(config.DBModeColumns)
	if err != nil {
		return false, err
	}
	for _, c := range modeColumns {
		if c == columnName {
			return true, nil
		}
	}
	return false, nil
}

func isAuthorColumn(columnName string) (bool, error) {
	authorColumns, err := config.GetStringSlice(config.DBAuthorColumns)
	if err != nil {
		return false, err
	}
	for _, c := range authorColumns {
		if c == columnName {
			return true, nil
		}
	}
	return false, nil
}
