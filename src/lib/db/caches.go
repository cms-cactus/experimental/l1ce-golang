package database

var foreignKeyCache = make(map[string]map[string]FkDefinition)
var primaryColumnCache = make(map[string]string)
var tableColumsCache = make(map[string]TableColumns)
var ownerCache = make(map[string]string)
var acessorCache = make(map[string]string)

// CleanCache clears all caches
func CleanCache() {
	foreignKeyCache = make(map[string]map[string]FkDefinition)
	primaryColumnCache = make(map[string]string)
	tableColumsCache = make(map[string]TableColumns)
	ownerCache = make(map[string]string)
	acessorCache = make(map[string]string)
}
