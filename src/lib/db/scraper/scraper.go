package scraper

import (
	"os"

	DB ".."
	config "../../config"
	git "../../git"
	l "../../log"
)

const legacyDir = ".deprecated/"

var log = l.GetLogger(l.Config{}, "scraper")

// FullScrape performs a full scrape for the given db name
func FullScrape() error {
	DB.CleanCache()
	CleanCache()
	dbName, err := config.GetString(config.DBDefault)
	if err != nil {
		return err
	}
	log.Info("scraping database", l.Extra{"database": dbName})
	if c, err := git.WsFromDefaultMaster(dbName); err != nil {
		return err
	} else if err := git.MakeWorkspace(c); err != nil {
		return err
	}
	if c, err := git.WsFromDefaultMaster("system/" + dbName); err != nil {
		return err
	} else if err := git.MakeWorkspace(c); err != nil {
		return err
	}

	for _, workspace := range []config.Workspace{
		config.UserToplevelWorkspace,
		config.UserSubsystemsWorkspace,
		config.UserTablesWorkspace,
		config.SystemTablesWorkspace,
		config.SystemTableConfigsWorkspace,
	} {
		folder, err := workspace.Folder()
		if err != nil {
			return err
		}
		recreateDir(folder)
		log.Warn("all files deleted in workspace", l.Extra{"path": folder})
	}

	topLevelTables, err := scrapeJobsFromConf()
	if err != nil {
		return err
	}
	for _, tableName := range topLevelTables {
		if err = TableScrapeRec(tableName); err != nil {
			return err
		}
	}

	if err := generateTopLevelLinks(); err != nil {
		return err
	}

	if err := generateSubsystemLinks(); err != nil {
		return err
	}

	DB.CleanCache()
	CleanCache()

	return nil
}

// TableName is the argument for PartialScrape()
type TableName string

// TableScrapeRec scrapes recursively starting from the given db table name
func TableScrapeRec(tableName TableName) error {

	// initialise the work list with the given table name
	keyOrTableScrapes := []KeyScrapeJob{{tableName, ""}}

	// go over the work list until it is empty
	for done := false; !done; done = len(keyOrTableScrapes) == 0 {

		// KeyScrape and TableScrape will return more work
		futureJobs := make([]KeyScrapeJob, 0)

		// go over the work list
		for _, job := range keyOrTableScrapes {
			// TableScrape() returns a KeyScrape() job for every subkey
			// it encounters, and a TableScrape() job for ever subtable
			if job.Key == "" {
				moreJobs, err := TableScrape(job.TableName)
				if err != nil {
					return err
				}
				futureJobs = append(futureJobs, moreJobs...)
			} else {
				// used to scrape a deprecated key, since TableScrape()
				// fetches only the latest key and fetches them internally
				// with keyscrape()
				// keyscrape does NOT return extra TableScrape() jobs,
				// it only returns extra jobs for additional keys it finds
				// an assumption is made that every table will be scraped by
				// TableScrape()s
				moreJobs, err := keyScrape(job)
				if err != nil {
					return err
				}
				futureJobs = append(futureJobs, moreJobs...)
			}
		}

		// all items in work list have been processed
		// now do it all over again for new work that popped up
		keyOrTableScrapes = futureJobs
	}
	return nil
}

// TableScrape scrapes a table and gives subsequent jobs for KeyScrape()s
// the returned job keys are the latest key of each mode in this table
// in addition, it also returns subsequent jobs for TableScrape()s
// one for each subtable encountered
func TableScrape(tableName TableName) ([]KeyScrapeJob, error) {

	tableConfigFilePath, err := getTableConfigFilePath(tableName)
	if _, err := os.Stat(tableConfigFilePath); !os.IsNotExist(err) {
		log.Trace("cache hit for table scrape", l.Extra{"tableName": tableName})
		return nil, nil
	}
	log.Info("scraping table", l.Extra{"table": tableName})

	tableColumns, err := DB.GetTableColumns(string(tableName))
	if err != nil {
		log.Error("cannot get table columns", l.Extra{"table": tableName})
		return nil, err
	}

	moreKeyScrapJobs := make([]KeyScrapeJob, 0)
	for _, columnDefinition := range tableColumns.Definitions {
		remoteTable := TableName(columnDefinition.Fk.RemoteTable)
		if remoteTable != "" {
			moreKeyScrapJobs = append(moreKeyScrapJobs, KeyScrapeJob{remoteTable, ""})
		}
	}

	modesAndKeys, err := getModesAndLatestKeys(tableName)
	if err != nil {
		log.Error("cannot get modes and latest keys", l.Extra{"table": tableName})
		return nil, err
	}

	for _, entry := range modesAndKeys {
		moreKeyScrapJobs = append(moreKeyScrapJobs, KeyScrapeJob{tableName, entry.latestKey})
	}

	if err := writeTableConfig(tableName); err != nil {
		log.Error("cannot write table config", l.Extra{"table": tableName})
		return nil, err
	}

	return moreKeyScrapJobs, nil
}

// KeyScrapeJob is the input for KeyScrape()
type KeyScrapeJob struct {
	TableName TableName
	Key       string // optional, used to scrape a deprecated key
}

// keyScrape scrapes a key, stores the raw key in the system/tables branch
// and the master branch if it is the latest key of its mode
// returns a list of additional scrape jobs, table scrape if this keys FK
// was referring to the latest key of its mode, another key scrape if it
// was pointing to a deprecated key
func keyScrape(job KeyScrapeJob) ([]KeyScrapeJob, error) {
	keyFolder, keyFileName, err := getKeyFilePath(config.SystemTablesWorkspace, job.TableName, job.Key)
	if err != nil {
		return nil, err
	}
	keyFilePath := keyFolder + keyFileName
	if _, err := os.Stat(keyFilePath); !os.IsNotExist(err) {
		log.Trace("cache hit for key scrape", l.Extra{"job": job})
		return nil, nil
	}
	log.Debug("scraping key", l.Extra{"job": job})
	moreWork := make([]KeyScrapeJob, 0)

	tableColumns, err := DB.GetTableColumns(string(job.TableName))
	if err != nil {
		log.Error("cannot get table columns", l.Extra{"table": job.TableName})
		return nil, err
	}
	if tableColumns.Mode != "" {
		modeTable := TableName(tableColumns.Definitions[tableColumns.Mode].Fk.RemoteTable)
		moreWork = append(moreWork, KeyScrapeJob{modeTable, ""})
	}

	rawRow, err := getKey(job.TableName, job.Key)
	if err != nil {
		log.Error("cannot get table key", l.Extra{"table": job.TableName, "key": job.Key})
		return nil, err
	}
	noClobsRow := make(map[string]string)
	clobEntries := make(map[string]string)
	for columnName, columnDefinition := range tableColumns.Definitions {
		value := rawRow[columnName]
		if columnDefinition.DataType == "CLOB" {
			clobEntries[columnName] = value
			value, err = writeClob(config.SystemTablesWorkspace, job.TableName, job.Key, columnName, []byte(value))
		}
		noClobsRow[columnName] = value
	}
	err = writeKeyFile(config.SystemTablesWorkspace, job.TableName, job.Key, keyFile{noClobsRow})
	if err != nil {
		return nil, err
	}

	// needed to check if this key is deprecated
	// if the key for this job is in fact the latest
	// we also write the moded file in refs/master
	modesAndKeys, err := getModesAndLatestKeys(job.TableName)
	if err != nil {
		log.Error("cannot get modes and latest keys", l.Extra{"table": job.TableName})
		return nil, err
	}

	var keyMode string
	if tableColumns.Mode == "" {
		keyMode = rawRow[tableColumns.Primary]
	} else {
		keyMode = rawRow[tableColumns.Mode]
	}
	thisIsLatestKey := modesAndKeys[keyMode].latestKey == rawRow[tableColumns.Primary]

	// when a key points to a deprecated subkey, the file will still
	// point to the mode name, but a separate lock file will specify
	// the exact (old) key it is pointing to
	lockFileEntries := make(map[string]string)

	// translate all keys into mode names
	modedRow := make(map[string]string)
	for columnName, columnValue := range noClobsRow {
		isIgnoredColumn, err := DB.IsIgnoredColumn(string(job.TableName), columnName)
		if err != nil {
			return nil, err
		}
		columnDefinition := tableColumns.Definitions[columnName]
		// if it is a column with a foreign key definition, and it's not empty
		if !isIgnoredColumn && columnValue != "" && columnDefinition.Fk.RemoteTable != "" {
			remoteTable := TableName(columnDefinition.Fk.RemoteTable)
			remoteRow, err := getKey(remoteTable, columnValue)
			if err != nil {
				return nil, err
			}
			remoteColumns, err := DB.GetTableColumns(string(remoteTable))
			if err != nil {
				log.Error("cannot get mode table row", l.Extra{"table": columnDefinition.Fk.RemoteTable, "key": job.Key})
				return nil, err
			}
			mode := remoteRow[remoteColumns.Mode]
			if remoteColumns.Mode == "" {
				mode = remoteRow[remoteColumns.Primary]
			}
			alias := makeAlias(columnName)
			modedRow[alias] = mode

			remoteModesAndKeys, err := getModesAndLatestKeys(remoteTable)
			if err != nil {
				return nil, err
			}
			latestRemoteKey := remoteModesAndKeys[mode].latestKey
			if columnValue != latestRemoteKey {
				lockFileEntries[alias] = columnValue

				// each entry for the lockfile is also an extra job for a
				// deprecated key that has to be scraped
				log.Debug("deprecated key used", l.Extra{"job": job, "column": columnName, "referenced table": remoteTable, "key": columnValue})
				moreWork = append(moreWork, KeyScrapeJob{remoteTable, columnValue})
			}
		}
	}

	// only create user key files when this is the latest key
	if thisIsLatestKey {
		err = writeKeyFile(config.UserTablesWorkspace, job.TableName, keyMode, keyFile{modedRow})
		if err != nil {
			log.Error("cannot create user key file", l.Extra{"job": job}, err)
			return nil, err
		}

		if len(lockFileEntries) != 0 {
			err = writeKeyFile(config.UserTablesWorkspace, job.TableName, keyMode+".lockfile", keyFile{lockFileEntries})
			if err != nil {
				log.Error("cannot create lockfile", l.Extra{"job": job}, err)
				return nil, err
			}
		}
	}
	return moreWork, nil
}

func recreateDir(path string) error {
	_ = os.RemoveAll(path) // on first run this will obviously fail
	return os.MkdirAll(path, os.ModePerm)
}
