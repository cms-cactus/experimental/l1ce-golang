package scraper

import (
	"errors"

	DB ".."
	l "../../log"
)

func getKey(keyTable TableName, key string) (map[string]string, error) {
	if keyTable == "" {
		return nil, errors.New("no key table provided to getKey")
	}
	if key == "" {
		return nil, errors.New("no key provided to getKey")
	}
	tableColumns, err := DB.GetTableColumns(string(keyTable))
	if err != nil {
		log.Error("cannot get table columns", l.Extra{"table": keyTable})
		return nil, err
	}

	fancySQL := "select * from " + string(keyTable) + " where " + tableColumns.Primary + " = '" + key + "'"
	rows, err := DB.QueryToMaps(string(keyTable), fancySQL)
	if err != nil {
		return nil, err
	}
	return rows[0], nil
}
