package scraper

import (
	"strings"
	"unicode"
)

// translate a column name into a more human readable alias
// a helpful command to find all current column names from the yaml files:
// find . -name '*\.yaml' -exec cat {} + | grep -oE '^  [^ ][^:]*:' | sort -u
func makeAlias(columnName string) string {
	if aliasCache == nil {
		aliasCache = make(map[string]string)
	}
	if aliasCache[columnName] != "" {
		return aliasCache[columnName]
	}
	alias := strings.ToLower(strings.Replace(columnName, "_", " ", -1))

	// proper capitalization of subsystem names
	alias = strings.Replace(alias, "csc", "CSC", -1)
	alias = strings.Replace(alias, "ugt", "uGT", -1)
	alias = strings.Replace(alias, "ugmt", "uGMT", -1)
	alias = strings.Replace(alias, "calol1", "CaloL1", -1)
	alias = strings.Replace(alias, "calol2", "CaloL2", -1)
	alias = strings.Replace(alias, "bmtf", "BMTF", -1)
	alias = strings.Replace(alias, "emtf", "EMTF", -1)
	alias = strings.Replace(alias, "omtf", "OMTF", -1)
	alias = strings.Replace(alias, "twinmux", "TwinMux", -1)
	alias = strings.Replace(alias, "cppf", "CPPF", -1)
	alias = strings.Replace(alias, "tcds", "tCDS", -1)
	alias = strings.Replace(alias, "hcal", "HCAL", -1)
	alias = strings.Replace(alias, "ecal", "ECAL", -1)
	alias = strings.Replace(alias, "dt", "DT", -1)
	alias = strings.Replace(alias, "rpc", "RPC", -1)
	alias = strings.Replace(alias, "amc13", "AMC13", -1)
	alias = strings.Replace(alias, "amc502", "AMC502", -1)
	alias = strings.Replace(alias, "mtf7", "MTF7", -1)
	alias = strings.Replace(alias, "tm7", "TM7", -1)
	alias = strings.Replace(alias, "tmt", "TMT", -1)
	alias = strings.Replace(alias, "mp7", "MP7", -1)

	// generic acronyms and expansions
	alias = strings.Replace(alias, "sw ", "software ", -1)
	alias = strings.Replace(alias, "hw ", "hardware ", -1)
	alias = strings.Replace(alias, "fw ", "firmware ", -1)
	alias = strings.Replace(alias, "es ", "ES ", -1)
	alias = strings.Replace(alias, "hf ", "HF ", -1)
	alias = strings.Replace(alias, "mp ", "MP ", -1)
	alias = strings.Replace(alias, "mps ", "MPS ", -1)
	alias = strings.Replace(alias, " egamma", " EGamma", -1)
	alias = strings.Replace(alias, "conf ", "config ", -1)
	alias = strings.Replace(alias, "hlt ", "HLT ", -1)
	alias = strings.Replace(alias, "cms ", "CMS ", -1)
	alias = strings.Replace(alias, "dqm ", "DQM ", -1)
	alias = strings.Replace(alias, "daq ", "DAQ ", -1)
	alias = strings.Replace(alias, "rs ", "run settings ", -1)
	alias = strings.Replace(alias, "trg ", "trigger ", -1)
	alias = strings.Replace(alias, " arch", " architecture", -1)

	// since we translate FK references to mode names, key becomes mode
	alias = strings.Replace(alias, " key", " mode", -1)

	if alias == "hw" {
		alias = "Hardware"
	}
	if alias == "conf" {
		alias = "Config"
	}
	if alias == "pi" {
		alias = "Partition Interface"
	}
	if alias == "ici" {
		alias = "ICI"
	}
	if alias == "mps" {
		alias = "MPS"
	}
	if alias == "luts" {
		alias = "LUTS"
	}
	if alias == "readoutmenu" {
		alias = "readout menu"
	}
	if alias == "techparams" {
		alias = "Tech parameters"
	}

	// make first letter uppercase
	runes := []rune(alias)
	if !unicode.IsUpper(runes[1]) {
		runes[0] = unicode.ToUpper(runes[0])
		alias = string(runes)
	}

	aliasCache[columnName] = alias
	return alias
}

func makeAliases(row map[string]string) map[string]string {
	aliases := make(map[string]string)
	for column := range row {
		alias := makeAlias(column)
		if alias != column {
			aliases[alias] = column
		}
	}
	return aliases
}
