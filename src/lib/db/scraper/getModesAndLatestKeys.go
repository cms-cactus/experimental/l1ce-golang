package scraper

import (
	DB ".."
	l "../../log"
)

type modeOrKeyEntry struct {
	latestKey   string
	description string
	namespaces  []string
	valid       bool
}

func getModesAndLatestKeys(keyTable TableName) (map[string]modeOrKeyEntry, error) {
	if modesWithLatestKeyCache[keyTable] != nil {
		return modesWithLatestKeyCache[keyTable], nil
	}

	keyTableColumns, err := DB.GetTableColumns(string(keyTable))
	if err != nil {
		return nil, err
	}
	keyHasValid := keyTableColumns.Definitions["VALID"].DataType != ""
	keyHasNamespace := keyTableColumns.Definitions["NAMESPACE"].DataType != ""

	fancySQL := ""

	connTable := string(keyTable)
	if keyTableColumns.Mode == "" {
		// no mode (edge case: L1_TRG_CONF_SEQUENCE)
		// modes are the keys themselves
		descriptionSelector := " description,"
		if keyTableColumns.Definitions["DESCRIPTION"].DataType == "" {
			descriptionSelector = ""
		}
		fancySQL = "select " + keyTableColumns.Primary + " key," + descriptionSelector + keyTableColumns.Primary + " modename"
		if keyHasValid {
			fancySQL += ", valid"
		} else {
			fancySQL += ",'Y' valid"
		}
		fancySQL += " from " + string(keyTable)
	} else {
		// key table has a mode column
		modeTable := keyTableColumns.Definitions[keyTableColumns.Mode].Fk.RemoteTable
		connTable = modeTable
		modeTableColumns, err := DB.GetTableColumns(modeTable)
		if err != nil {
			return nil, err
		}
		keyTableOwner, _, err := DB.GetTableOwner(string(keyTable))
		if err != nil {
			return nil, err
		}

		hasDescription := modeTableColumns.Definitions["DESCRIPTION"].DataType != ""
		modeHasValid := modeTableColumns.Definitions["VALID"].DataType != ""
		modeHasNamespace := modeTableColumns.Definitions["NAMESPACE"].DataType != ""

		fancySQL = "SELECT "
		if hasDescription {
			fancySQL += "description, "
		}
		if modeHasValid {
			fancySQL += "m.valid modevalid, "
		} else {
			fancySQL += "'Y' modevalid, "
		}
		if modeHasNamespace {
			fancySQL += "m.namespace namespace, "
		} else if keyHasNamespace {
			fancySQL += "k.namespace namespace, "
		} else {
			fancySQL += "null namespace, "
		}
		fancySQL += `k.modename, k.key, k.valid keyvalid,
		(case when k.valid = m.valid and m.valid = 'Y' then 'Y' else 'N' end) valid
FROM ` + modeTable + ` m
inner join (
	select ` + keyTableColumns.Mode + ` modename,`
		if keyHasNamespace {
			fancySQL += " namespace,"
		}
		if keyHasValid {
			fancySQL += `
	max(valid) keep (dense_rank last order by version) valid,`
		} else {
			fancySQL += `
	'Y' valid,`
		}
		fancySQL += `
	max(version) keep (dense_rank last order by version) version,
	max(` + keyTableColumns.Primary + `) keep (dense_rank last order by version) key
from ` + keyTableOwner + "." + string(keyTable) + `
group by ` + keyTableColumns.Mode
		if keyHasNamespace {
			fancySQL += ", namespace"
		}
		fancySQL += `
) k on m.` + modeTableColumns.Primary + ` = k.modename`

	}

	rows, err := DB.QueryToMaps(connTable, fancySQL)
	if err != nil {
		return nil, err
	}
	modes := make(map[string]modeOrKeyEntry)
	for _, row := range rows {
		modeName := row["MODENAME"]
		oldEntry := modes[modeName]
		namespaces := append(oldEntry.namespaces, row["NAMESPACE"])
		modes[modeName] = modeOrKeyEntry{
			latestKey:   row["KEY"],
			description: row["DESCRIPTION"],
			namespaces:  namespaces,
			valid:       row["VALID"] == "Y",
		}
	}

	log.Trace("got modes and latest keys", l.Extra{"table": keyTable, "modes": modes})

	modesWithLatestKeyCache[keyTable] = modes
	return modes, nil

}
