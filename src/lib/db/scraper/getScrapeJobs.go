package scraper

import (
	"fmt"

	config "../../config"
)

// scrapeJobsFromConf generates a list of scrape jobs derived from the config file
func scrapeJobsFromConf() ([]TableName, error) {
	var rawjobs interface{}
	var err error
	if rawjobs, err = config.Get(config.DBScraperJobs); err != nil {
		return nil, err
	}

	var jobs []interface{}
	var ok bool
	if jobs, ok = rawjobs.([]interface{}); !ok {
		return nil, config.WrongConfigError(config.DBScraperJobs, "not an array")
	}

	result := make([]TableName, 0)
	for i := range jobs {
		var job map[interface{}]interface{}
		if job, ok = jobs[i].(map[interface{}]interface{}); !ok {
			return nil, config.WrongConfigError(config.DBScraperJobs, string(i)+" is not a map")
		}
		startTable := fmt.Sprintf("%v", job["table"])
		result = append(result, TableName(startTable))
	}
	return result, nil
}
