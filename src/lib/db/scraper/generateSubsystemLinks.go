package scraper

import (
	"fmt"
	"os"
	"strings"

	config "../../config"
	l "../../log"
)

// generateSubsystemLinks produces a symbolic link directory structure
// for easy access to various parts of the configuration
func generateSubsystemLinks() error {
	folder, err := config.UserSubsystemsWorkspace.Folder()
	if err != nil {
		return err
	}
	log.Info("generating subsystem links", l.Extra{"path": folder})

	var subsystemsConf []interface{}
	{
		var ok bool
		subsystemsConfRaw, err := config.Get(config.DBScraperSubsystems)
		if err != nil {
			return err
		}
		if subsystemsConf, ok = subsystemsConfRaw.([]interface{}); !ok {
			return config.WrongConfigError(config.DBScraperSubsystems, "not an array")
		}
	}
	for i := range subsystemsConf {
		var subsystemConf map[interface{}]interface{}
		var ok bool
		if subsystemConf, ok = subsystemsConf[i].(map[interface{}]interface{}); !ok {
			return config.WrongConfigError(config.DBScraperSubsystems, string(i)+" is not a map")
		}
		var subsystemNames []interface{}
		if subsystemNames, ok = subsystemConf["names"].([]interface{}); !ok {
			return config.WrongConfigError(config.DBScraperSubsystems, string(i)+".names is not an array")
		}
		var sections []interface{}
		if sections, ok = subsystemConf["sections"].([]interface{}); !ok {
			return config.WrongConfigError(config.DBScraperSubsystems, string(i)+".sections is not an array")
		}
		for _, subsystemName := range subsystemNames {
			subsystemName := fmt.Sprintf("%v", subsystemName)
			subsystemPath := folder + subsystemName + "/"
			err := os.Mkdir(subsystemPath, os.ModePerm)
			if err != nil && !os.IsExist(err) {
				return err
			}

			for a := range sections {
				var sectionConf map[interface{}]interface{}
				if sectionConf, ok = sections[a].(map[interface{}]interface{}); !ok {
					return config.WrongConfigError(config.DBScraperSubsystems, string(i)+".section."+string(a)+" is not a map")
				}
				sectionName := fmt.Sprintf("%v", sectionConf["name"])
				sectionTarget := fmt.Sprintf("%v", sectionConf["value"])
				sectionTarget = strings.ReplaceAll(sectionTarget, "%name%", subsystemName)
				err = os.Symlink("../../top-level/"+sectionTarget, subsystemPath+sectionName)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}
