package scraper

import (
	"errors"
	"os"

	"gopkg.in/yaml.v2"

	DB ".."
	config "../../config"
)

// TableConfig holds the scraped configuration of a table
type TableConfig struct {
	Table   TableName
	Owner   string
	Columns TableConfigColumns
}

// TableConfigColumns is part of TableConfig
type TableConfigColumns struct {
	Primary     string
	Mode        string            `yaml:",omitempty"`
	Author      string            `yaml:",omitempty"`
	Aliases     map[string]string `yaml:",omitempty"`
	Definitions map[string]DB.ColumnDefinition
}

func getTableConfigFilePath(tableName TableName) (string, error) {
	if tableName == "" {
		return "", errors.New("no table passed to getTableConfigFilePath")
	}
	basePath, err := config.SystemTableConfigsWorkspace.Folder()
	if err != nil {
		return "", err
	}
	return basePath + string(tableName) + ".yaml", nil
}

func writeTableConfig(tableName TableName) error {
	tableColumns, err := DB.GetTableColumns(string(tableName))
	if err != nil {
		return err
	}

	owner, _, err := DB.GetTableOwner(string(tableName))
	if err != nil {
		return err
	}

	c := TableConfig{
		tableName, owner,
		TableConfigColumns{
			tableColumns.Primary, tableColumns.Mode,
			tableColumns.Author, nil, tableColumns.Definitions}}
	var aliases = make(map[string]string)
	for columnName := range tableColumns.Definitions {
		if r, err := DB.IsIgnoredColumn(string(tableName), columnName); err != nil {
			return err
		} else if !r {
			alias := makeAlias(columnName)
			aliases[alias] = columnName
		}
	}
	c.Columns.Aliases = aliases
	bytes, err := yaml.Marshal(c)

	filePath, err := getTableConfigFilePath(tableName)
	if err != nil {
		return err
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	_, err = file.Write(bytes)
	if err != nil {
		return err
	}
	return file.Close()
}
