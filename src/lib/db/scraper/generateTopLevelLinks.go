package scraper

import (
	"os"
	"path/filepath"

	DB ".."
	config "../../config"
	l "../../log"
)

type scrapeJobYaml struct {
	table    string
	toplevel string
}

func generateTopLevelLinks() error {
	toplevelFolder, err := config.UserToplevelWorkspace.Folder()
	if err != nil {
		return err
	}
	log.Info("generating top-level links", l.Extra{"path": toplevelFolder})

	scrapeJobs := make([]scrapeJobYaml, 0)
	{
		var ok bool
		var scrapeJobsInterface []interface{}
		scrapeJobsRaw, err := config.Get(config.DBScraperJobs)
		if err != nil {
			return err
		}
		if scrapeJobsInterface, ok = scrapeJobsRaw.([]interface{}); !ok {
			return config.WrongConfigError(config.DBScraperSubsystems, "not an array")
		}
		for _, scrapeJobInterface := range scrapeJobsInterface {
			var scrapeJobMap map[interface{}]interface{}
			if scrapeJobMap, ok = scrapeJobInterface.(map[interface{}]interface{}); !ok {
				return config.WrongConfigError(config.DBScraperSubsystems, "not a map")
			}
			var scrapeJob scrapeJobYaml
			var key interface{}
			var val string
			key = "table"
			if val, ok = scrapeJobMap[key].(string); !ok {
				return config.WrongConfigError(config.DBScraperSubsystems, "not a scrape job")
			}
			scrapeJob.table = val
			key = "toplevel"
			if val, ok = scrapeJobMap[key].(string); !ok {
				return config.WrongConfigError(config.DBScraperSubsystems, "not a scrape job")
			}
			scrapeJob.toplevel = val
			if scrapeJob.table == "" || scrapeJob.toplevel == "" {
				return config.WrongConfigError(config.DBScraperSubsystems, "not a scrape job")
			}
			scrapeJobs = append(scrapeJobs, scrapeJob)
		}
	}

	tablesFolder, err := config.UserTablesWorkspace.Folder()
	if err != nil {
		return err
	}
	for _, scrapeJob := range scrapeJobs {
		filePath := toplevelFolder + scrapeJob.toplevel
		link := tablesFolder + scrapeJob.table
		rel, err := filepath.Rel(toplevelFolder, link)
		if err != nil {
			return err
		}
		log.Debug("writing top-level link", l.Extra{"file": filePath, "dest": rel})
		os.Remove(filePath)
		err = os.Symlink(rel, filePath)
		if err != nil {
			return err
		}
	}

	file, err := os.Open(tablesFolder)
	if err != nil {
		return err
	}
	defer file.Close()

	tableNames, err := file.Readdirnames(0)
	if err != nil {
		return err
	}
	for _, tableName := range tableNames {
		tableColumns, err := DB.GetTableColumns(string(tableName))
		if err != nil {
			return err
		}
		for columnName, columnDef := range tableColumns.Definitions {
			remoteTable := columnDef.Fk.RemoteTable
			isIgnoredColumn, err := DB.IsIgnoredColumn(tableName, columnName)
			if err != nil {
				return err
			}
			if tableColumns.Mode == columnName {
				isIgnoredColumn = false
			}
			if remoteTable != "" && !isIgnoredColumn {
				alias := makeAlias(columnName)
				filePath := tablesFolder + tableName + "/" + alias
				rel := "../" + remoteTable
				os.Remove(filePath)
				err = os.Symlink(rel, filePath)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}
