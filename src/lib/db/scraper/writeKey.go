package scraper

import (
	"os"
	"strings"

	config "../../config"
	l "../../log"
	"gopkg.in/yaml.v2"
)

type keyFile struct {
	Key map[string]string
}

func writeKeyFile(ws config.Workspace, tableName TableName, modeName string, data keyFile) error {
	folder, fileName, err := getKeyFilePath(ws, tableName, modeName)
	if err != nil {
		return err
	}
	path := folder + fileName
	log.Debug("writing file", l.Extra{"path": path})

	if err := os.MkdirAll(folder, os.ModePerm); err != nil {
		log.Error("cannot create directory tree", l.Extra{"folder": folder}, err)
		return err
	}
	file, err := os.Create(path)
	if err != nil {
		log.Error("cannot create file", l.Extra{"path": path}, err)
		return err
	}
	bytes, err := yaml.Marshal(data)
	if err != nil {
		log.Error("cannot generate yaml", l.Extra{"path": path, "obj": data}, err)
		return err
	}
	_, err = file.Write(bytes)
	if err != nil {
		log.Error("cannot write file", l.Extra{"path": path}, err)
		return err
	}

	err = file.Close()
	if err != nil {
		log.Error("cannot close file", l.Extra{"path": path}, err)
		return err
	}
	return nil
}

func writeClob(ws config.Workspace, tableName TableName, modeName string, columnName string, data []byte) (string, error) {
	folder, err := ws.Folder()
	if err != nil {
		return "", err
	}
	folder = folder + string(tableName) + "/"

	extention := "clob"
	if len(data) > 0 {
		if string(data[0]) == "<" {
			extention = "xml"
		} else if string(data[0]) == "{" {
			extention = "json"
		}
	}
	fileName := sanitizeForFS(modeName) + "." + columnName + "." + extention
	path := folder + fileName

	if err := os.MkdirAll(folder, os.ModePerm); err != nil {
		log.Error("cannot create directory tree", l.Extra{"folder": folder}, err)
		return fileName, err
	}
	file, err := os.Create(path)
	if err != nil {
		log.Error("cannot create file", l.Extra{"path": path}, err)
		return fileName, err
	}
	_, err = file.Write(data)
	if err != nil {
		log.Error("cannot write file", l.Extra{"path": path}, err)
		return fileName, err
	}

	err = file.Close()
	if err != nil {
		log.Error("cannot close file", l.Extra{"path": path}, err)
		return fileName, err
	}
	return fileName, nil
}

func getKeyFilePath(ws config.Workspace, tableName TableName, modeName string) (string, string, error) {
	folder, err := ws.Folder()
	if err != nil {
		return "", "", err
	}
	folder = folder + string(tableName) + "/"

	fileName := sanitizeForFS(modeName) + ".yaml"

	return folder, fileName, nil
}

func sanitizeForFS(s string) string {
	s = strings.ReplaceAll(s, "/", "%2F")
	s = strings.ReplaceAll(s, "\\", "%5C")
	return s
}
