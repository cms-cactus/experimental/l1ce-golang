package scraper

var modesWithLatestKeyCache = make(map[TableName]map[string]modeOrKeyEntry)
var aliasCache map[string]string

// CleanCache clears all caches
func CleanCache() {
	modesWithLatestKeyCache = make(map[TableName]map[string]modeOrKeyEntry)
	aliasCache = make(map[string]string)
}
