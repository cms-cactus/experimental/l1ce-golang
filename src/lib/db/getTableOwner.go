package database

import (
	"errors"

	config "../config"
	l "../log"
	_ "gopkg.in/goracle.v2" // goracle driver
)

// GetTableOwner gives you the owner name of the given table name
func GetTableOwner(tablename string) (string, string, error) {
	if tablename == "" {
		return "", "", errors.New("no table name given to getTableOwner")
	}
	if ownerCache[tablename] != "" && acessorCache[tablename] != "" {
		return ownerCache[tablename], acessorCache[tablename], nil
	}

	defaultUser, err := config.GetString(config.DBDefaultUser)
	if err != nil {
		return "", "", err
	}
	conn, err := Connect(defaultUser)
	if err != nil {
		return "", "", err
	}
	defer conn.Close()

	rows, err := conn.Query("select owner from all_tables where table_name = :1 and owner in "+MakeLegalOwnersSQLSet(), tablename)
	if err != nil {
		return "", "", err
	}
	defer rows.Close()
	rows2, err := conn.Query("select owner from all_views where view_name = :1 and owner in "+MakeLegalOwnersSQLSet(), tablename)
	if err != nil {
		return "", "", err
	}
	defer rows2.Close()

	owner := ""
	for rows.Next() {
		rows.Scan(&owner)
	}
	for rows2.Next() {
		rows.Scan(&owner)
	}
	if owner == "" {
		return "", "", errors.New("no owner found for table " + tablename)
	}
	// we have edge cases where we have owner X, but we have no credentials for X and we're supposed to
	// login with user Y and query X.tablename
	// X is the owner, Y is the accessor
	accessor := owner
	if pass, err := getDBCredentials(owner); err != nil || pass == "" {
		accessor = defaultUser
		if owner == "CMS_TCDS_TRG_PRO" {
			accessor = "CMS_TRG_L1_CONF"
		}

		log.Debug("table owner has no configured credentials, overriding owner", l.Extra{"table": tablename, "owner": owner, "override": accessor})
	}
	ownerCache[tablename] = owner
	acessorCache[tablename] = accessor
	return owner, accessor, nil
}
