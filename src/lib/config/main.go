package config

import (
	"os"
	"strings"

	l "../../lib/log"
	ExitCodes "../exitcodes"
	"github.com/spf13/viper"
)

var log = l.GetLogger(l.Config{}, "cmdflags")

// ConfigKey describes a config key
type ConfigKey struct {
	Name        string
	Description string
}

// ConfigError extends error
type ConfigError struct {
	s       string
	Key     ConfigKey
	KeyName string
	EnvName string
}

func (e *ConfigError) Error() string {
	return e.s
}

// EnvPrefix : env variables with name <prefix>_<key> are automatically read
// and override default values and values specified in config files
// dots '.' are to be replaced with underscores '_'
// examples: git.user -> L1CE_GIT_USER
const EnvPrefix = "L1CE"

// list of config keys to be used with viper.Get(<name>)
var (
	Loglevel               = ConfigKey{"logging.level", "log level"}
	LogFormat              = ConfigKey{"logging.format", "log format"}
	GitRepository          = ConfigKey{"git.repository", "remote repository"}
	GitUser                = ConfigKey{"git.user", "git username"}
	GitAPIKey              = ConfigKey{"git.apikey", "git API key"}
	GitClonePath           = ConfigKey{"git.clonepath", "clone path"}
	DBNames                = ConfigKey{"database.names", "valid database names"}
	DBAliases              = ConfigKey{"database.aliases", "aliases for use in db config"}
	DBDefault              = ConfigKey{"database.default", "what database name to use by default"}
	DBCredentialsType      = ConfigKey{"database.credentials.type", "database credentials backend"}
	DBCredentialsPath      = ConfigKey{"database.credentials.path", "database credentials search path"}
	DBUsers                = ConfigKey{"database.validUsers", "known valid database login names"}
	DBDefaultUser          = ConfigKey{"database.defaultUser", "default database login name"}
	DBTableOwners          = ConfigKey{"database.validTableOwners", "known valid database table owners"}
	DBIgnoredColumns       = ConfigKey{"database.ignoredColumns", "ignored database table columns"}
	DBStaticAliases        = ConfigKey{"database.staticAliases", "static database column aliases"}
	DBModeColumns          = ConfigKey{"database.modeColumns", "known database table mode columns"}
	DBAuthorColumns        = ConfigKey{"database.authorColumns", "known database table author columns"}
	DBScraperLegacyEnabled = ConfigKey{"database.scraper.scrapeLegacyKeys", "database scraper legacy scraping enabled"}
	DBScraperSubsystems    = ConfigKey{"database.scraper.subsystems", "list of subsystems for database scraper"}
	DBScraperJobs          = ConfigKey{"database.scraper.scrapeJobs", "list of database scraper jobs"}
)

// GetEnvKey gives you the environment variable name to be used
// to override a config key
var GetEnvKey = func(confKey string) string {
	if confKey == "" {
		return confKey
	}
	return EnvPrefix + "_" + strings.ToUpper(strings.ReplaceAll(confKey, ".", "_"))
}

func isPresent(c ConfigKey) error {
	if !viper.IsSet(c.Name) {
		return MissingConfigError(c)
	}
	return nil
}

// Get wraps around viper.Get(), returns an error if not set
func Get(confkey ConfigKey) (interface{}, error) {
	if err := isPresent(confkey); err != nil {
		return nil, err
	}
	return viper.Get(confkey.Name), nil
}

// GetString wraps around viper.GetString(), returns an error if not set
func GetString(confkey ConfigKey) (string, error) {
	if err := isPresent(confkey); err != nil {
		return "", err
	}
	return viper.GetString(confkey.Name), nil
}

// GetBool wraps around viper.GetBool(), returns an error if not set
func GetBool(confkey ConfigKey) (bool, error) {
	if err := isPresent(confkey); err != nil {
		return false, err
	}
	return viper.GetBool(confkey.Name), nil
}

// GetStringMapString wraps around viper.GetStringMapString(), returns an error if not set
func GetStringMapString(confkey ConfigKey) (map[string]string, error) {
	if err := isPresent(confkey); err != nil {
		return nil, err
	}
	return viper.GetStringMapString(confkey.Name), nil
}

// GetStringSlice wraps around viper.GetStringSlice(), returns an error if not set
func GetStringSlice(confkey ConfigKey) ([]string, error) {
	if err := isPresent(confkey); err != nil {
		return nil, err
	}
	return viper.GetStringSlice(confkey.Name), nil
}

// EnsureString uses GetString but exits the program if confkey is unset
func EnsureString(confkey ConfigKey) string {
	s, err := GetString(confkey)
	if err != nil {
		FatalMissingConf(err)
	}
	return s
}

// MissingConfigError for reporting a missing config key
func MissingConfigError(k ConfigKey) *ConfigError {
	var d = k.Description
	if d == "" {
		d = k.Name
	}
	envName := GetEnvKey(k.Name)
	return &ConfigError{
		s:       "no " + d + " configured, set $key in config or set environment variable $env",
		Key:     k,
		KeyName: k.Name,
		EnvName: envName,
	}
}

// WrongConfigError for reporting a bad config key
func WrongConfigError(k ConfigKey, msg string) *ConfigError {
	var d = k.Description
	if d == "" {
		d = k.Name
	}
	m := "Invalid " + d + " configuration"
	if msg != "" {
		m += ". " + msg
	}
	return &ConfigError{
		s:       m,
		Key:     k,
		KeyName: k.Name,
		EnvName: GetEnvKey(k.Name),
	}
}

// LogMissingConf logs a standard log message
func LogMissingConf(e error) {
	extra := l.Extra{}
	if e, ok := e.(*ConfigError); ok {
		extra["key"] = e.KeyName
		extra["env"] = e.EnvName
	}
	log.Fatal(e.Error(), extra)
}

// FatalMissingConf invokes LogMissingConf() and exits the program
func FatalMissingConf(e error) {
	LogMissingConf(e)
	os.Exit(ExitCodes.ConfigError)
}
