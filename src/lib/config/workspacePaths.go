package config

import "errors"

// Workspace is a location within the configs git repository directory structure
type Workspace int

const (
	// MasterWorkspace points to the location of the master branch
	MasterWorkspace Workspace = iota
	// UserWorkspace points to the user-editable workspace of the configured database
	UserWorkspace
	// UserToplevelWorkspace points to the user-editable top-level workspace of the configured database
	UserToplevelWorkspace
	// UserSubsystemsWorkspace points to the user-editable subsystems workspace of the configured database
	UserSubsystemsWorkspace
	// UserTablesWorkspace points to the user-editable tables workspace of the configured database
	UserTablesWorkspace
	// SystemTablesWorkspace points to the root path where the system stores raw keys
	SystemTablesWorkspace
	// SystemTableConfigsWorkspace points to the root path where the system stores table configuration
	SystemTableConfigsWorkspace
)

// Folder retrieves the filesystem folder for a workspace
func (w Workspace) Folder() (string, error) {
	_gitClonePath, err := GetString(GitClonePath)
	if err != nil {
		return "", err
	}
	dbName, err := GetDbName()
	if err != nil {
		return "", err
	}

	switch w {
	case MasterWorkspace:
		return _gitClonePath + "/master/", nil
	case UserWorkspace:
		return _gitClonePath + "/" + dbName, nil
	case UserToplevelWorkspace:
		return _gitClonePath + "/" + dbName + "/top-level/", nil
	case UserSubsystemsWorkspace:
		return _gitClonePath + "/" + dbName + "/subsystems/", nil
	case UserTablesWorkspace:
		return _gitClonePath + "/" + dbName + "/tables/", nil
	case SystemTablesWorkspace:
		return _gitClonePath + "/system/" + dbName + "/keys/", nil
	case SystemTableConfigsWorkspace:
		return _gitClonePath + "/system/" + dbName + "/config/", nil
	default:
		return "", errors.New("no such workspace name")
	}
}
