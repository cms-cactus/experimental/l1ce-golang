package config

import "strings"

// ValidateDbName checks the given db name with the list of valid
// db names from config
func ValidateDbName(dbName string) error {
	dbNames, err := GetStringSlice(DBNames)
	if err != nil {
		return err
	}
	for _, validDB := range dbNames {
		if validDB == dbName {
			return nil
		}
	}
	return WrongConfigError(DBDefault, dbName+" not in list of valid DB names")
}

func safeDBName(dbName string) string {
	if strings.Contains(dbName, ":") {
		return strings.Split(dbName, ":")[0]
	}
	return dbName
}

// GetDbName retrieves the db name from config and checks with the list of
// valid db names in config
// needs to be checked in case CLI overrides db name
func GetDbName() (string, error) {
	dbName, err := GetString(DBDefault)
	if err != nil {
		return "", err
	}

	return safeDBName(dbName), ValidateDbName(dbName)
}
