package static

import (
	"bytes"
	"errors"
	"io"
	"os/exec"
	"strings"
	"time"

	l "../log"
	scriptassets "./scripts"
)

var log = l.GetLogger(l.Config{}, "git")

// ExecuteScript loads and runs the specified script with the specified arguments
func ExecuteScript(scriptPath string, arguments ...string) (string, string, error) {
	script, err := scriptassets.Assets.Open(scriptPath)
	if err != nil {
		return "", "", err
	}
	defer script.Close()

	fileinfo, err := script.Stat()
	if err != nil {
		return "", "", err
	}
	size := fileinfo.Size()

	buffer := make([]byte, size)
	bytesRead, err := script.Read(buffer)

	// gzip.Read says Clients should treat data
	// returned by Read as tentative until they receive the io.EOF
	// marking the end of the data
	// while the other readers in vfsgen return EOF only if you try to
	// read more data than available
	gZipBug := err == io.EOF && size == int64(bytesRead)
	if err != nil && !gZipBug {
		return "", "", err
	}

	cmd := exec.Command("bash", append([]string{"-s"}, arguments...)...)
	cmd.Stdin = strings.NewReader(strictBash(string(buffer)))
	stdout, stderr := bytes.NewBuffer(nil), bytes.NewBuffer(nil)
	cmd.Stdout, cmd.Stderr = stdout, stderr
	err = cmd.Start()

	done := make(chan error)
	go func() { done <- cmd.Wait() }()

	timeout := time.After(20 * time.Second)

	select {
	case <-timeout:
		cmd.Process.Kill()
		return string(stdout.Bytes()), string(stderr.Bytes()), errors.New("Process timed out")
	case err := <-done:
		return string(stdout.Bytes()), string(stderr.Bytes()), err
	}
}

func strictBash(input string) string {
	return `
set -o errexit
set -o nounset
set -o pipefail
` + input
}
