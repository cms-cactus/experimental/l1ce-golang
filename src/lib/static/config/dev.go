// +build !production

package static

import "net/http"

// Assets contains project assets.
var Assets http.FileSystem = http.Dir("static/config")
