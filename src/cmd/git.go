package cmd

import (
	ConfKeys "../lib/config"
	"github.com/spf13/cobra"
)

var gitCmd = &cobra.Command{
	Use:   "git",
	Short: "L1CE interaction with git",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
}

func addGitFlags(cmd *cobra.Command) {
	cmd.Flags().String(ConfKeys.GitRepository.Name, "https://gitlab.cern.ch/cms-cactus/web/l1ce-git.git", "set git repo")
	cmd.Flags().String(ConfKeys.GitUser.Name, "cactus", "set git user")
	cmd.Flags().String(ConfKeys.GitAPIKey.Name, "", "set git API key")
}

func init() {
	rootCmd.AddCommand(gitCmd)
}
