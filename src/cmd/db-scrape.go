package cmd

import (
	"os"

	config "../lib/config"
	scraper "../lib/db/scraper"
	"github.com/spf13/cobra"
)

var partialScrapePath *string

var dbScrapeCmd = &cobra.Command{
	Use:     "scrape",
	Aliases: []string{"rebuild", "togit"},
	Short:   "L1CE git backend initialisation",
	Long:    `Scrapes the database and builds a file structure for git`,
	Run: func(cmd *cobra.Command, args []string) {
		_, err := config.GetDbName()
		stopIfErr("cannot get database name", err)
		// if *partialScrapePath != "" {
		// 	stopIfErr("cannot perform scrape", scraper.PartialScrape(dbName, *partialScrapePath))
		// } else {
		stopIfErr("cannot perform scrape", scraper.FullScrape())
		// }
		log.Info("done", nil)
	},
}

func init() {
	dbCmd.AddCommand(dbScrapeCmd)
	addDbFlags(dbScrapeCmd)
	partialScrapePath = dbScrapeCmd.Flags().String("partial.folder", "", "when defined, re-scrapes the specified sub-folder of a previous scrape")
}

func stop(msg string, err error) {
	log.Fatal(msg, nil, err)
	os.Exit(1)
}
func stopIfErr(msg string, err error) {
	if err != nil {
		stop(msg, err)
	}
}
