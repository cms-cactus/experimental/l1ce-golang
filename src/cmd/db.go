package cmd

import (
	Confkeys "../lib/config"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var dbCmd = &cobra.Command{
	Use:     "db",
	Aliases: []string{"database"},
	Short:   "L1CE interaction with the database",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
}

func addDbFlags(cmd *cobra.Command) {
	cmd.Flags().String("database.name", "INT2R", "set db name")
	viper.BindPFlag(Confkeys.DBDefault.Name, cmd.Flags().Lookup("database.name"))
}

func init() {
	rootCmd.AddCommand(dbCmd)
}
