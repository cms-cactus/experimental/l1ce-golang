package cmd

import (
	"os"
	"strings"

	Confkeys "../lib/config"
	ExitCodes "../lib/exitcodes"
	l "../lib/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh/terminal"
)

var log = l.GetLogger(l.Config{}, "cmd")

var cfgFile string
var logFormat string

// read https://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf
// page 9 specifies why to prefer /etc/opt
var defaultConfigPath = "/etc/opt/cactus/l1ce/config/"
var defaultConfigFile = "l1ce"

var rootCmd = &cobra.Command{
	Use:   "L1CE",
	Short: "CMS Level-1 Configuration Editor",
	Long: `The Level-1 Configuration Editor is a tool that assist you with
any task related to the provisioning of configuration for the
CMS Level-1 Trigger at P5`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config.file", "", "config file (default "+defaultConfigPath+defaultConfigFile+".(yaml|toml|json))")
	rootCmd.PersistentFlags().StringP(Confkeys.Loglevel.Name, "l", "info", "set log level")
	defaultLogFormat := "json"
	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		defaultLogFormat = "text"
	}
	rootCmd.PersistentFlags().StringVar(&logFormat, Confkeys.LogFormat.Name, defaultLogFormat, "set log format")
	viper.BindPFlag(Confkeys.Loglevel.Name, rootCmd.PersistentFlags().Lookup(Confkeys.Loglevel.Name))
	viper.BindPFlag(Confkeys.LogFormat.Name, rootCmd.PersistentFlags().Lookup(Confkeys.LogFormat.Name))
	l.SetLogFormat(defaultLogFormat)
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(defaultConfigPath)
		viper.AddConfigPath(".")
		viper.SetConfigName(defaultConfigFile)
	}

	// read in env variables, env variables override config file
	// example: 'loglevel: info' in config.yaml will be overriden by command
	//          'L1CE_LOGLEVEL=ERROR go run main.go'
	viper.SetEnvPrefix(Confkeys.EnvPrefix)
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			if cfgFile == "" {
				cwd, _ := os.Getwd()
				log.Warn("no config file found", l.Extra{"cwd": cwd})
			} else {
				log.Fatal("specified config file not found", l.Extra{"config file path": cfgFile})
				os.Exit(ExitCodes.ConfigError)
			}
		} else {
			log.Fatal("an error occurred while reading config file", l.Extra{"file": viper.ConfigFileUsed()}, err)
			os.Exit(ExitCodes.ConfigError)
		}

	} else {
		formatSetSuccess := l.SetLogFormat(viper.GetString(Confkeys.LogFormat.Name))
		log.Info("loaded config file", l.Extra{"file": viper.ConfigFileUsed()})
		if !formatSetSuccess {
			log.Error("log format set failed", l.Extra{"requested log format": viper.GetString(Confkeys.LogFormat.Name)})
		}
		loglevel := viper.GetString(Confkeys.Loglevel.Name)
		log.Info("setting log level", l.Extra{"log level": loglevel, "old log level": l.GetLogLevel()})
		if !l.SetLogLevel(loglevel) {
			log.Error("loglevel set failed", l.Extra{"requested log level": loglevel, "using log level": l.GetLogLevel()})
		}
	}
}
