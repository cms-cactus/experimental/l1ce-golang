package cmd

import (
	"os"

	db "../lib/db"
	Exitcodes "../lib/exitcodes"
	"github.com/spf13/cobra"
)

var dbTestCmd = &cobra.Command{
	Use:     "test-connections",
	Aliases: []string{"connect", "test-connect"},
	Short:   "L1CE git backend initialisation",
	Long:    `Clones the configured git repository`,
	Run: func(cmd *cobra.Command, args []string) {
		err := db.TestConnections()
		if err != nil {
			log.Fatal("Database connection failed", nil, err)
			os.Exit(Exitcodes.ConfigError)
		}
	},
}

func init() {
	dbCmd.AddCommand(dbTestCmd)
}
