package cmd

import (
	"os"

	ConfKeys "../lib/config"
	Exitcodes "../lib/exitcodes"
	Git "../lib/git"
	"github.com/spf13/cobra"
)

var workspaceRemoveCmd = &cobra.Command{
	Use:     "delete [workspace name]",
	Aliases: []string{"del", "remove", "rm"},
	Short:   "L1CE git backend initialisation",
	Long:    `Clones the configured git repository`,
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		nameFlag := args[0]
		c, e := Git.WsFromDefaultMaster(nameFlag)
		if e != nil {
			ConfKeys.FatalMissingConf(e)
		}

		err := Git.DeleteWorkspace(c)
		if err != nil {
			log.Fatal("Initialisation failed", nil, err)
			os.Exit(Exitcodes.CannotCreateFile)
		}
	},
}

func init() {
	workspaceCmd.AddCommand(workspaceRemoveCmd)
}
