package cmd

import (
	"fmt"
	"os"
	"text/tabwriter"

	ConfKeys "../lib/config"
	Exitcodes "../lib/exitcodes"
	Git "../lib/git"
	"github.com/spf13/cobra"
)

var quietFlag *bool

var workspaceListCmd = &cobra.Command{
	Use:   "list",
	Short: "L1CE git backend initialisation",
	Long:  `Clones the configured git repository`,
	Run: func(cmd *cobra.Command, args []string) {

		master, e := Git.DefaultWsMaster()
		if e != nil {
			ConfKeys.FatalMissingConf(e)
		}

		workspaces, err := Git.ListWorkspaces(master)
		if err != nil {
			log.Fatal("Initialisation failed", nil, err)
			os.Exit(Exitcodes.CannotCreateFile)
		}

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0)
		if !*quietFlag {
			fmt.Fprintln(w, "NAME\tPATH\tWORK IN PROGRESS")
		}
		for _, workspace := range workspaces {
			if *quietFlag {
				fmt.Println(workspace.Name)
			} else {
				dirty := "false"
				if workspace.Dirty {
					dirty = "true"
				}
				fmt.Fprintln(w, workspace.Name+"\t"+workspace.Path+"\t"+dirty)
			}
		}
		w.Flush()
	},
}

func init() {
	workspaceCmd.AddCommand(workspaceListCmd)
	quietFlag = workspaceListCmd.Flags().BoolP("quiet", "Q", false, "only display workspace names")
}
