package cmd

import (
	"os"

	ConfKeys "../lib/config"
	Exitcodes "../lib/exitcodes"
	Git "../lib/git"
	"github.com/spf13/cobra"
)

var gitInitCmd = &cobra.Command{
	Use:     "init",
	Aliases: []string{"clone"},
	Short:   "L1CE git backend initialisation",
	Long:    `Clones the configured git repository`,
	Run: func(cmd *cobra.Command, args []string) {

		c, e := Git.DefaultCloneConfig()
		if e != nil {
			ConfKeys.FatalMissingConf(e)
		}

		err := Git.Clone(c)
		if err != nil {
			log.Fatal("Initialisation failed", nil, err)
			os.Exit(Exitcodes.ConfigError)
		}
	},
}

func init() {
	gitCmd.AddCommand(gitInitCmd)
	addGitFlags(gitInitCmd)
	gitInitCmd.Flags().String(ConfKeys.GitClonePath.Name, "https://gitlab.cern.ch/cms-cactus/web/l1ce-git.git", "set clone destination")
}
