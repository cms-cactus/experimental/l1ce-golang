package main

import (
	"./cmd"
	Logger "./lib/log"
)

const appname = "L1CE"

var _ = Logger.Init(Logger.Config{}, appname)

func main() {
	cmd.Execute()
}

// 	bashPath, err := exec.LookPath("bash")
// 	if err != nil {
// 		log.Fatal("no bash found", nil, err)
// 		return
// 	}
// 	log.Info("bash found", l.Extra{"path": bashPath})

// 	gitPath, err := exec.LookPath("git")
// 	if err != nil {
// 		log.Fatal("no git found", nil, err)
// 		return
// 	}
// 	log.Info("git found", l.Extra{"path": gitPath})
