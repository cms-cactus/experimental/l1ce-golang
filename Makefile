SHELL:=/bin/bash
.DEFAULT_GOAL := build/l1ce

# /web gets compiled by vue-cli into /src/static/web
web_src_files := $(shell find -type f | grep -e '\./web/' | grep -v -e 'node_modules' | sed 's|\s|\\ |g')
static/web: $(web_src_files)
	cd web && npx vue-cli-service build --mode production

# web/production.go and scripts/production.go get compiled from the /static folder
static_files := $(shell find -type f | grep -e '\./static/' | sed 's|\s|\\ |g')
src/lib/static/web/production.go: $(static_files) static/web
	go generate ./static

# /build/l1ce gets compiled from /src
src_files := $(shell find -type f | grep -e '\./src/' | sed 's|\s|\\ |g')
build/l1ce: $(src_files) src/lib/static/web/production.go
	mkdir -p build
	go build -o build/l1ce -tags=production src/main.go

.PHONY: clean
clean:
	rm -rf build
	rm -f src/static/*/production.go
	rm -rf static/web

# in dev mode (go run src/main.go args), static files are read from /static rather than a production.go
# when working on the web interface, this is a useful helper alongside 'go run src/main.go serve'
.PHONY: webwatch
webwatch:
	cd web && npx vue-cli-service build --watch