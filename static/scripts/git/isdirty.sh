cd "$1"

STATUS=$(git status --porcelain --ignore-submodules=dirty 2> /dev/null | tail -n1)
if [[ -n $STATUS ]]; then
  echo -n "dirty"
else
  echo -n "clean"
fi