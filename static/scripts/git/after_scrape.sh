#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'

# A scrape is a reset to reflect the actual state of the database
# Can be used for both initialization from empty state, or
# recovery from a corrupt state
# Therefore, the note system is very simple
# - take each note file and look at the corresponding blob
# - if it has no note, apply it
# - if it has a note, and the IDs match, ignore
# - if it has a note, and the IDs mismatch, apply

# This script does not apply changes recursively, which might
# be needed if for example someone changes a BMTF Config key without
# updating the parent key.
# This is expected to be picked up later on at the merge to master.
# Therefore, this script must not run directly on a DB branch.

cd "$1"
NOTES_SUFFIX=".notes.json"

# TO_ADD_FILES=()
# CHANGED_FILES=`git ls-files --modified`
# for file in $CHANGED_FILES; do
#   if [[ "$file" == *.notes.json ]]; then
#     >&2 echo "found notes file in list of changed files?"
#     exit 1
#   fi
#   TO_ADD_FILES+=("$file")
# done
# DELETED_FILES=`git ls-files --deleted`
# for file in $DELETED_FILES; do
#   if [[ "$file" == *.notes.json ]]; then
#     >&2 echo "found notes file in list of deleted files?"
#     exit 1
#   fi
#   TO_ADD_FILES+=("$file")
# done

# NEW_FILES=`git ls-files --other`
# NOTE_FILES=()
# for file in $NEW_FILES; do
#   if [[ "$file" == *.notes.json ]]; then
#     NOTE_FILES+=("$file")
#   else
#     TO_ADD_FILES+=("$file")
#   fi
# done

# git add -A "${TO_ADD_FILES[@]}"






# git add -A "*\.yaml" "*.clob" subsystems
NOTES_FILES=`git ls-files --other "*.notes.json"`

MISSED_FILES=`git ls-files --deleted --modified --others --exclude "*$NOTES_SUFFIX"`
if [[ "$MISSED_FILES" != "" ]]; then
  >&2 echo "not all files are accounted for"
  exit 1
fi

# git commit --message "L1CE Scrape" --author="L1CE <cms.l1t.l1ce@cern.ch>" > /dev/null
COMMIT_HASH=`git rev-parse HEAD`
echo $COMMIT_HASH
# echo $NOTES_FILES | xargs --delimiter "\n" sed --in-place --expression "s|notcommittedyet|$COMMIT_HASH|"

NOTES_SUFFIX_L=`echo -n $NOTES_SUFFIX | wc -c`
for note_file in $NOTES_FILES; do
  # file=`echo $note_file | sed "s|$NOTES_SUFFIX|.yaml|"`
  note=`cat $note_file | sed "s|notcommittedyet|$COMMIT_HASH|"`
  file=`echo ${note_file::(-$NOTES_SUFFIX_L)}.yaml`
  # echo "$note_file -> $file"
  file_hash=`git hash-object "$file"`
  if existing_note=`git notes --ref database show $file_hash 2> /dev/null`; then
    echo "hash exists for $file"
    echo $existing_note
    echo $note
  fi
done

# i=0
# for line in `git diff-tree --no-commit-id --abbrev=64 -r HEAD`; do
#   i=$(( $i + 1 ))
#   if (( $i % 2 )); then
#     # hash=`echo "$line" | cut -d ' ' -f4`
#     hash=${line:56:40}
#     type=${line:97:1}
#   else
#     file=$line
#     # echo "$hash -> $file"

#     # TODO
#     # deletion is only allowed if it was moved to 
#     if [[ "$type" == "A" || "$type" == "M" ]]; then
#       if git notes show $hash &> /dev/null; then
#         echo "hash exists for $file"
#       else
#         echo $file
#       fi
#     else
#       >&2 echo "not supported change type $type"
#       exit 1
#     fi
#   fi
# done