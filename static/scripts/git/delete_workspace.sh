usage () {
	echo "usage:" $@
	exit 127
}

die () {
	echo $@
	exit 128
}

if test $# -lt 2 || test $# -gt 3
then
	usage "$0 <repository> <workdir> [<branch>]"
fi

orig_git=$1
workdir=$2
branch=$3

if ! test -d "$workdir"
then
  die "specified workdir does not exist '$workdir'!"
fi

rm -rf $workdir
cd $orig_git
git branch -D $branch