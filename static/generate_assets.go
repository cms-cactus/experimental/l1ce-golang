//go:generate go run generate_assets.go

package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/shurcooL/vfsgen"
)

func main() {
	dirs := []string{"scripts", "web", "config"}
	for _, dir := range dirs {
		err := vfsgen.Generate(http.Dir(dir), vfsgen.Options{
			PackageName:  "static",
			BuildTags:    "production",
			VariableName: "Assets",
			Filename:     "../src/lib/static/" + dir + "/production.go",
		})
		checkErr(err)
	}
}

func checkErr(err error) {
	if err != nil {
		fmt.Println("error while generating static assets:", err)
		os.Exit(1)
	}
}
